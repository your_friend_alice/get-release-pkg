package cache

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"time"

	"gitlab.com/your_friend_alice/get-release-pkg/pkg/release"
)

func dir() (string, error) {
	dir, err := os.UserCacheDir()
	if err != nil {
		return "", fmt.Errorf("Error getting user cache directory:\n  %w", err)
	}
	return filepath.Join(dir, "your_friend_alice", "get-release-pkg"), nil
}

func mkdir() error {
	dir, err := dir()
	if err != nil {
		return fmt.Errorf("Error getting cache directory:\n  %w", err)
	}
	err = os.MkdirAll(dir, 0o775)
	if err != nil {
		return fmt.Errorf("Error creating cache directory:\n  %w", err)
	}
	return nil
}

func nameFor(url string) string {
	sum := sha256.Sum256([]byte(url))
	return string(hex.EncodeToString(sum[:])) + ".json"
}

func pathFor(url string) (string, error) {
	dir, err := dir()
	if err != nil {
		return "", fmt.Errorf("Error getting cache directory:\n  %w", err)
	}
	return filepath.Join(dir, nameFor(url)), nil
}

func SaveRelease(r release.Release) error {
	dir, err := dir()
	if err != nil {
		return fmt.Errorf("Error getting cache directory:\n  %w", err)
	}
	err = mkdir()
	if err != nil {
		return fmt.Errorf("Error creating cache directory:\n  %w", err)
	}
	filename := nameFor(r.Url)
	tmpfile, err := os.CreateTemp(dir, filename+"-")
	defer tmpfile.Close()
	encoder := json.NewEncoder(tmpfile)
	err = encoder.Encode(r)
	if err != nil {
		os.Remove(tmpfile.Name())
		return fmt.Errorf("Error saving file %q:\n  %w", tmpfile.Name(), err)
	}
	err = os.Rename(tmpfile.Name(), filepath.Join(dir, filename))
	if err != nil {
		os.Remove(tmpfile.Name())
		return fmt.Errorf("Error moving temporary file %q to %q:\n  %w", tmpfile.Name(), filename, err)
	}
	return nil
}

func GetRelease(url string, ttl time.Duration) (release.Release, bool, error) {
	path, err := pathFor(url)
	if err != nil {
		return release.Release{}, false, fmt.Errorf("Error getting path for URL %q:\n  %w", url, err)
	}
	info, err := os.Stat(path)
	if os.IsNotExist(err) {
		// no entry
		return release.Release{}, false, nil
	} else if err != nil {
		return release.Release{}, false, fmt.Errorf("Error getting info on cache file %q for URL %q:\n  %w", path, url, err)
	} else if info.ModTime().Add(ttl).Before(time.Now()) {
		// expired entry
		return release.Release{}, false, nil
	}
	file, err := os.Open(path)
	defer file.Close()
	if err != nil {
		return release.Release{}, false, fmt.Errorf("Error opening cache file %q for URL %q:\n  %w", path, url, err)
	}
	decoder := json.NewDecoder(file)
	out := release.Release{}
	err = decoder.Decode(&out)
	if err != nil {
		return release.Release{}, false, fmt.Errorf("Error decoding cache file %q for URL %q:\n  %w", path, url, err)
	}
	return out, true, nil
}

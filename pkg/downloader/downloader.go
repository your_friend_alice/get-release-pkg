package downloader

import (
	"fmt"
	"io"
	"io/fs"
	"net/http"
	neturl "net/url"
	"os"
	"path"
	"time"

	"github.com/schollz/progressbar/v3"
)

func mode(chmod bool) fs.FileMode {
	if chmod {
		return 0o755
	} else {
		return 0o644
	}
}

func Download(url, outfile string, force, chmod bool) error {
	resp, err := http.Get(url)
	if err != nil {
		return fmt.Errorf("Error sending HTTPS request to %q:\n  %w", url, err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return fmt.Errorf("Request to %q returned status code %d", url, resp.StatusCode)
	}
	var f *os.File
	if outfile == "" {
		u, err := neturl.Parse(url)
		if err != nil {
			return fmt.Errorf("Error parsing download URL %q:\n  %w", url, err)
		}
		outfile = path.Base(u.Path)
	}
	if outfile == "-" {
		_, err = io.Copy(os.Stdout, resp.Body)
		if err != nil {
			return fmt.Errorf("Error writing body from request %q to stdout:\n  %w", url, err)
		}
	} else {
		// first, check if the file exists and is the same size as the request body.
		info, err := os.Stat(outfile)
		if err != nil && !os.IsNotExist(err) {
			return fmt.Errorf("Cannot stat output file %q:\n  %w", outfile, err)
		} else if force || os.IsNotExist(err) || info.Size() != resp.ContentLength {
			f, err = os.OpenFile(outfile, os.O_CREATE|os.O_EXCL|os.O_WRONLY, mode(chmod))
			if err != nil {
				return fmt.Errorf("Error opening output file %q:\n  %w", outfile, err)
			}
			bar := progressbar.NewOptions(
				int(resp.ContentLength),
				progressbar.OptionSetDescription(fmt.Sprintf("Download %q", outfile)),
				progressbar.OptionShowBytes(true),
				progressbar.OptionThrottle(10*time.Millisecond),
				progressbar.OptionUseANSICodes(true),
				progressbar.OptionSetWriter(os.Stderr),
			)
			_, err = io.Copy(io.MultiWriter(f, bar), resp.Body)
			if err != nil {
				return fmt.Errorf("Error writing body from request %q to file %q:\n  %w", url, outfile, err)
			}
			fmt.Println(outfile)
		} else {
			fmt.Fprintf(os.Stderr, "File %q already downloaded\n", outfile)
			fmt.Println(outfile)
		}
	}
	return nil
}

package github

import (
	"encoding/json"
	"fmt"
	"net/http"
	neturl "net/url"
	"regexp"
	"strings"
	"time"

	"gitlab.com/your_friend_alice/get-release-pkg/pkg/cache"
	"gitlab.com/your_friend_alice/get-release-pkg/pkg/release"
)

func getRepo(url string) (string, error) {
	parts, err := neturl.Parse(url)
	if err != nil {
		return "", fmt.Errorf("Error parsing URL %q:\n  %w", url, err)
	} else if parts.Host != "github.com" || parts.Scheme != "https" {
		return "", fmt.Errorf("URL %q is not a github URL", url)
	}
	path := strings.Trim(parts.Path, "/")
	if len(strings.Split(path, "/")) != 2 {
		return "", fmt.Errorf("Url %q does not fit the required format of \"https://github.com/<user>/<repo>\"", url)
	}
	return path, nil
}

type tmpRelease struct {
	Name   string
	Assets []tmpAsset
}
type tmpAsset struct {
	Name string
	Type string `json:"content_type"`
	Url  string `json:"browser_download_url"`
}

func getRelease(url, version string, ttl time.Duration) (release.Release, error) {
	repo, err := getRepo(url)
	if err != nil {
		return release.Release{}, fmt.Errorf("Error getting name for repo %q:\n  %w", url, err)
	}
	if version != "latest" {
		version = "tags/" + version
	}
	requestUrl := "https://api.github.com/repos/" + repo + "/releases/" + version
	rel, ok, err := cache.GetRelease(requestUrl, ttl)
	if err != nil {
		return release.Release{}, fmt.Errorf("Error looking up cached release for repo %q:\n  %w", url, err)
	} else if ok {
		return rel, nil
	}
	resp, err := http.Get(requestUrl)
	if err != nil {
		return release.Release{}, fmt.Errorf("Error sending HTTPS request to %q for repo %q:\n  %w", requestUrl, url, err)
	}
	if resp.StatusCode != 200 {
		return release.Release{}, fmt.Errorf("Request to %q for repo %q return status code %d", requestUrl, url, resp.StatusCode)
	}
	defer resp.Body.Close()
	decoder := json.NewDecoder(resp.Body)
	tmp := tmpRelease{}
	err = decoder.Decode(&tmp)
	if err != nil {
		return release.Release{}, fmt.Errorf("Error decoding response from %q for repo %q:\n  %w", requestUrl, url, err)
	}
	assets := make([]release.Asset, 0, len(tmp.Assets))
	for _, asset := range tmp.Assets {
		assets = append(assets, release.Asset{Name: asset.Name, Type: asset.Type, Url: asset.Url})
	}
	rel = release.Release{
		Url:     url,
		Version: strings.TrimPrefix(tmp.Name, "v"),
		Assets:  assets,
	}
	err = cache.SaveRelease(rel)
	if err != nil {
		return release.Release{}, fmt.Errorf("Errorf saving response from %q for repo %q to cache:\n  %w", requestUrl, url, err)
	}
	return rel, nil
}

func Get(url, version, format, arch, os string, ttl time.Duration, re *regexp.Regexp) (string, error) {
	release, err := getRelease(url, version, ttl)
	if err != nil {
		return "", fmt.Errorf("Error getting latest release for repo %q:\n  %w", url, err)
	}
	asset, err := release.GetAsset(format, arch, os, re)
	if err != nil {
		return "", fmt.Errorf("Error finding a suitable asset in release %q for repo %q:\n  %w", release.Version, url, err)
	}
	return asset.Url, nil
}

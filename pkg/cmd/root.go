package cmd

import (
	"fmt"
	"os"
	"regexp"
	"runtime"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/your_friend_alice/get-release-pkg/pkg/downloader"
	"gitlab.com/your_friend_alice/get-release-pkg/pkg/github"
)

var rootCmd = &cobra.Command{
	Use:          "get-release-pkg",
	Short:        "Download packages from github releases",
	Long:         "Download packages from github releases",
	Args:         cobra.NoArgs,
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		re, err := regexp.Compile(Regexp)
		if err != nil {
			return fmt.Errorf("Invalid regexp: %w", err)
		}
		result, err := github.Get(Url, Version, Format, Arch, Os, Ttl, re)
		if err != nil {
			return err
		}
		return downloader.Download(result, Out, Force, Chmod && Format == "appimage")
	},
}

var (
	Url, Format, Arch, Os, Out, Regexp, Version string
	Force, Chmod                                bool
	Ttl                                         time.Duration
)

func init() {
	rootCmd.CompletionOptions.DisableDefaultCmd = false
	rootCmd.AddCommand(&cobra.Command{})
	rootCmd.Flags().StringVarP(&Url, "url", "u", "", "Repo URL")
	rootCmd.MarkFlagRequired("url")
	rootCmd.Flags().StringVarP(&Version, "version", "v", "latest", "What version of the package to download")
	rootCmd.Flags().StringVarP(&Format, "format", "f", "", "What type of package to look for")
	rootCmd.MarkFlagRequired("format")
	rootCmd.Flags().StringVarP(&Arch, "arch", "a", runtime.GOARCH, "What architecture to find a package for")
	rootCmd.Flags().StringVarP(&Os, "os", "", runtime.GOOS, "What OS to find a package for")
	rootCmd.Flags().StringVarP(&Out, "out", "o", "", "Output file, \"-\" for STDOUT")
	rootCmd.Flags().DurationVarP(&Ttl, "ttl", "t", 8*time.Hour, "Max cache entry age")
	rootCmd.Flags().BoolVarP(&Chmod, "chmod", "", false, "Make output file executable, if format is appimage")
	rootCmd.Flags().BoolVarP(&Force, "force", "", false, "Always re-download files, even if they already exist")
	rootCmd.Flags().StringVarP(&Regexp, "regexp", "r", ".*", "Additional regular expression to match against possible package names")
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}

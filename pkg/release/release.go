package release

import (
	"fmt"
	"regexp"
	"strings"
)

type Fuzzy int

const (
	NO Fuzzy = iota
	MAYBE
	YES
)

type Format struct {
	Suffixes []string
	Mimes    []string
}

var (
	FORMATS = map[string]Format{
		"tar.gz": Format{
			Suffixes: []string{".tar.gz", ".tgz"},
			Mimes:    []string{"application/tar+gzip", "application/x-gzip", "application/x-tgz"},
		},
		"deb": Format{
			Suffixes: []string{".deb"},
			Mimes:    []string{"application/vnd.debian.binary-package", "application/x-debian-package"},
		},
		"appimage": Format{
			Suffixes: []string{".AppImage"},
			Mimes:    []string{"application/octet-stream"},
		},
	}
	ARCHES = map[string]*regexp.Regexp{
		"386":   regexp.MustCompile(`\bx86|i?386|i?686\b`),
		"amd64": regexp.MustCompile(`\bx86[_-]64|amd64|x64\b`),
		"arm":   regexp.MustCompile(`\barm(32)?\b`),
		"arm64": regexp.MustCompile(`\barm64\b`),
	}
	OSES = map[string]*regexp.Regexp{
		"linux":   regexp.MustCompile(`\blinux\b`),
		"macos":   regexp.MustCompile(`\bmacos|darwin\b`),
		"windows": regexp.MustCompile(`\bwin32|windows\b`),
	}
)

type Release struct {
	Url     string
	Version string
	Assets  []Asset
}

func (r Release) GetAsset(format, arch, os string, re *regexp.Regexp) (Asset, error) {
	evaluatedAssets := []evaluatedAsset{}
	for _, asset := range r.Assets {
		if re.MatchString(asset.Name) {
			var err error
			a := evaluatedAsset{Asset: asset}
			a.IsFormat, err = asset.isFormat(format)
			if err != nil {
				return Asset{}, err
			}
			a.IsArch, err = asset.isArch(arch)
			if err != nil {
				return Asset{}, err
			}
			a.IsOs, err = asset.isOs(os)
			if err != nil {
				return Asset{}, err
			}
			if a.Nos() == 0 {
				evaluatedAssets = append(evaluatedAssets, a)
			}
		}
	}
	for _, yeses := range []int{3, 2, 1} {
		for _, a := range evaluatedAssets {
			if a.Yeses() >= yeses {
				return a.Asset, nil
			}
		}
	}
	return Asset{}, fmt.Errorf("No release assets matched the requirements")
}

type Asset struct {
	Name string
	Type string
	Url  string
}

type evaluatedAsset struct {
	Asset    Asset
	IsFormat Fuzzy
	IsArch   Fuzzy
	IsOs     Fuzzy
}

func (a evaluatedAsset) Yeses() int {
	out := 0
	if a.IsFormat == YES {
		out++
	}
	if a.IsArch == YES {
		out++
	}
	if a.IsOs == YES {
		out++
	}
	return out
}

func (a evaluatedAsset) Maybes() int {
	out := 0
	if a.IsFormat == MAYBE {
		out++
	}
	if a.IsArch == MAYBE {
		out++
	}
	if a.IsOs == MAYBE {
		out++
	}
	return out
}

func (a evaluatedAsset) Nos() int {
	out := 0
	if a.IsFormat == NO {
		out++
	}
	if a.IsArch == NO {
		out++
	}
	if a.IsOs == NO {
		out++
	}
	return out
}

func ValidateFormat(f string) error {
	_, ok := FORMATS[f]
	if !ok {
		formats := make([]string, 0, len(FORMATS))
		for format, _ := range FORMATS {
			formats = append(formats, format)
		}
		return fmt.Errorf("Unsupported format: %q. Supported formats: %v", f, formats)
	}
	return nil
}

func (a Asset) isFormat(f string) (Fuzzy, error) {
	f = strings.ToLower(f)
	err := ValidateFormat(f)
	if err != nil {
		return NO, err
	}
	suffF := a.suffixFormat()
	mimeF := a.mimeFormat()
	if suffF != "" && suffF != f {
		return NO, nil
	} else if mimeF != "" && mimeF != f {
		return NO, nil
	} else if suffF == f || mimeF == f {
		return YES, nil
	}
	return MAYBE, nil
}

func (a Asset) suffixFormat() string {
	for name, format := range FORMATS {
		for _, suffix := range format.Suffixes {
			if strings.HasSuffix(strings.ToLower(a.Name), suffix) {
				return name
			}
		}
	}
	return ""
}

func (a Asset) mimeFormat() string {
	for name, format := range FORMATS {
		for _, mime := range format.Mimes {
			if strings.HasSuffix(strings.ToLower(a.Type), mime) {
				return name
			}
		}
	}
	return ""
}

func ValidateArch(f string) error {
	f = strings.ToLower(f)
	_, ok := ARCHES[f]
	if !ok {
		arches := make([]string, 0, len(ARCHES))
		for arch, _ := range ARCHES {
			arches = append(arches, arch)
		}
		return fmt.Errorf("Unsupported architecture: %q. Supported architectures: %v", f, arches)
	}
	return nil
}

func (a Asset) isArch(arch string) (Fuzzy, error) {
	err := ValidateArch(arch)
	if err != nil {
		return NO, err
	}
	if ARCHES[arch].MatchString(a.Name) {
		return YES, nil
	}
	for _, arch := range ARCHES {
		if arch.MatchString(a.Name) {
			return NO, nil
		}
	}
	return MAYBE, nil
}

func ValidateOs(f string) error {
	f = strings.ToLower(f)
	_, ok := OSES[f]
	if !ok {
		oses := make([]string, 0, len(OSES))
		for os, _ := range OSES {
			oses = append(oses, os)
		}
		return fmt.Errorf("Unsupported OS: %q. Supported OSes: %v", f, oses)
	}
	return nil
}

func (a Asset) isOs(os string) (Fuzzy, error) {
	err := ValidateOs(os)
	if err != nil {
		return NO, err
	}
	if OSES[os].MatchString(a.Name) {
		return YES, nil
	}
	for _, os := range OSES {
		if os.MatchString(a.Name) {
			return NO, nil
		}
	}
	return MAYBE, nil
}

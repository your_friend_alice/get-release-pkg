module gitlab.com/your_friend_alice/get-release-pkg

go 1.14

require (
	github.com/schollz/progressbar/v3 v3.7.6 // indirect
	github.com/spf13/cobra v1.1.3-0.20210218152603-eb3b6397b1b5
)
